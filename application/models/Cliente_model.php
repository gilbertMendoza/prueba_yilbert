<?php
class Cliente_model extends CI_model
{
	function guardar($data){
		$this->db->insert("clientes",$data);

		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}
	function mostrar($valor){
		$this->db->like("nombre",$valor);
		$consulta = $this->db->get("clientes");
		return $consulta->result();
	}

	function actualizar($id,$data){
		$this->db->where('id', $id);
		$this->db->update('clientes', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}

	function eliminar($id){
		$this->db->where('id', $id);
		$this->db->delete('clientes');
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}

}
