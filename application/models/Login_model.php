<?php
class Login_model extends CI_model
{
  public function __construct()
  {
    $this->load->database();
  }
  public function login($username, $password)
  {
    $query = $this->db->select('users.*,roles.nombre AS rol')
    ->join('roles','roles.id = users.id_rol')
    ->get_where('users', array('username' => $username));

    if($query->num_rows() == 1)
    {
      $row=$query->row();
      $password_hash=md5($password);
      if ($password_hash == $row->password)
      {
        $data=['user_data'=>[
          'id'=>$row->id,
          'name'=>$row->name,
          'address'=>$row->address,
          'phone'=>$row->phone,
          'username'=>$row->username,
          'id_rol'=>$row->id_rol,
          'rol'=>$row->rol,
          ]];
        $this->session->set_userdata($data);
        return true;
      }
    }
    $this->session->unset_userdata('user_data');
    return false;
  }
}
