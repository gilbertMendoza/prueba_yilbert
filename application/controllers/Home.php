
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('id'))
    {
      redirect('private_area');
    }
    $this->load->library('form_validation');
    $this->load->model('Register_model');
    $this->load->model('Cliente_model');
  }

  public function index()
  {
    $data = $this->session->userdata();
    $this->load->view('pages/home',$data);
  }

  public function listarClientes()
  {
    if ($this->input->is_ajax_request()) {
      $buscar = $this->input->post("buscar");
      $datos = $this->Cliente_model->mostrar($buscar);
      echo json_encode($datos);
    } else {
      show_404();
    }
  }

}

?>
