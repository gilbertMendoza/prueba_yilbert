<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('id'))
    {
      redirect('private_area');
    }
    $this->load->library('form_validation');
    $this->load->model('register_model');
  }

  function index()
  {
    $this->load->view('register');
  }

  function validation()
  {
    $this->form_validation->set_rules('name', 'Nombre', 'required|trim');
    $this->form_validation->set_rules('address', 'Dirección', 'required|trim');
    $this->form_validation->set_rules('phone', 'Teléfono', 'required|trim');
    $this->form_validation->set_rules('username', 'Usuario', 'required|trim|is_unique[users.username]');
    $this->form_validation->set_rules('password', 'Contraseña', 'required');

    $this->form_validation->set_message('required', '%s es requerido');
    $this->form_validation->set_message('is_unique', '%s ya se encuentra registrado');

    if($this->form_validation->run())
    {
      // $password_hash=password_hash($this->input->post('password'), PASSWORD_BCRYPT);
      $password_hash=md5($this->input->post('password'));
      $data = array(
        'name'  => $this->input->post('name'),
        'address'  => $this->input->post('address'),
        'phone'  => $this->input->post('phone'),
        'username'  => $this->input->post('username'),
        'password' => $password_hash,
        'id_rol ' => 2
      );
      $id = $this->register_model->insert($data);
      if($id > 0)
      {
        // $subject = "Please verify email for login";
        // $message = "
        // <p>Hi ".$this->input->post('user_name')."</p>
        // <p>This is email verification mail from Codeigniter Login Register system. For complete registration process and login into system. First you want to verify you email by click this <a href='".base_url()."register/verify_email/".$verification_key."'>link</a>.</p>
        // <p>Once you click this link your email will be verified and you can login into system.</p>
        // <p>Thanks,</p>
        // ";
        // $config = array(
        //   'protocol'  => 'smtp',
        //   'smtp_host' => 'smtpout.secureserver.net',
        //   'smtp_port' => 80,
        //   'smtp_user'  => 'xxxxxxx',
        //   'smtp_pass'  => 'xxxxxxx',
        //   'mailtype'  => 'html',
        //   'charset'    => 'iso-8859-1',
        //   'wordwrap'   => TRUE
        // );
        // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        // $this->email->from('info@webslesson.info');
        // $this->email->to($this->input->post('user_email'));
        // $this->email->subject($subject);
        // $this->email->message($message);
        // if($this->email->send())
        // {
          $this->session->set_flashdata('message', 'Se registro el usuario correctamente');
          // $this->load->view('templates/header');
          // $this->load->view('register');
          // $this->load->view('templates/footer');
          redirect('login');
        // }
      }else {
        $this->session->set_flashdata('message', 'Hubo un error al registrar el usuario');
        $this->index();
      }
    } else {
      $this->index();
    }
  }

  function verify_email()
  {
    if($this->uri->segment(3))
    {
      $verification_key = $this->uri->segment(3);
      if($this->register_model->verify_email($verification_key))
      {
        $data['message'] = '<h1 align="center">Your Email has been successfully verified, now you can login from <a href="'.base_url().'login">here</a></h1>';
      }
      else
      {
        $data['message'] = '<h1 align="center">Invalid Link</h1>';
      }
      $this->load->view('email_verification', $data);
    }
  }

}

?>
