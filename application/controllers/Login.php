<?php
class Login extends CI_controller
{
    public function __construct()
    {
      parent::__construct();
      if($this->session->userdata('id'))
      {
        redirect('private_area');
      }
      $this->load->helper('url');
      $this->load->helper('form');
      $this->load->library('form_validation');
    }

    public function index()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $this->form_validation->set_rules('username' ,'Usuario', 'required');
        $this->form_validation->set_rules('password' ,'Contraseña', 'required|callback_verifica');

        $this->form_validation->set_message('required', '%s es requerido');

        if($this->form_validation->run())
        {
          redirect('home');
        } else {

          $this->load->view('login');
        }
    }

    public function verifica()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if($this->login_model->login($username, $password))
        {

          redirect('home');

        } else {

          $this->session->set_flashdata('verifica', 'Credenciales incorrectas');
          $this->load->view('login');

        }
    }

    function logout()
    {
      $data = $this->session->all_userdata();
      foreach($data as $row => $rows_value)
      {
        $this->session->unset_userdata($row);
      }
      redirect('login');
    }
}
