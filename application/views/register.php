<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pages/auth.css">
</head>

<body>
<div id="auth">

  <div class="row h-100">
    <div class="col-lg-5 col-12">
      <div id="auth-left">
        <h1 class="auth-title">Registrate</h1>
        <span class="text-success">
          <?php
          $error_msg=$this->session->flashdata('message');
          if($error_msg){
            echo $error_msg;
          }
          ?>
        </span>
        <form method="post" action="<?php echo base_url(); ?>register/validation">
          <div class="form-group position-relative has-icon-left mb-4">
            <input type="text" class="form-control form-control-xl" name="name" placeholder="Nombre" value="<?php echo set_value('name'); ?>">
            <small class="text-danger"><?php echo form_error('name'); ?></small>
          </div>
          <div class="form-group position-relative has-icon-left mb-4">
            <input type="text" class="form-control form-control-xl" name="address" placeholder="Dirección" value="<?php echo set_value('address'); ?>">
            <small class="text-danger"><?php echo form_error('address'); ?></small>
          </div>
          <div class="form-group position-relative has-icon-left mb-4">
            <input type="text" class="form-control form-control-xl" name="phone" placeholder="Teléfono" value="<?php echo set_value('phone'); ?>">
            <small class="text-danger"><?php echo form_error('phone'); ?></small>
          </div>
          <div class="form-group position-relative has-icon-left mb-4">
            <input type="text" class="form-control form-control-xl" name="username" placeholder="Username"  value="<?php echo set_value('username'); ?>">
            <small class="text-danger"><?php echo form_error('username'); ?></small>
          </div>
          <div class="form-group position-relative has-icon-left mb-4">
            <input type="password" class="form-control form-control-xl" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
            <small class="text-danger"><?php echo form_error('password'); ?></small>
          </div>
          <button type="submit" class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Registrate</button>
        </form>
        <div class="text-center mt-5 text-lg fs-4">
          <p class='text-gray-600'>
            <a href="<?php echo base_url(); ?>login" class="font-bold">Login</a>.
          </p>
        </div>
      </div>
      </div>
      <div class="col-lg-7 d-none d-lg-block">
        <div id="auth-right">

        </div>
      </div>
    </div>

  </div>
</body>
</html>
