<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pages/auth.css">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
    $( document ).ready(function() {

      window.base_url = "<?php echo base_url(); ?>";

      window.info = {
        id : "<?php echo $user_data['id']; ?>",
        name : "<?php echo $user_data['name']; ?>",
        address : "<?php echo $user_data['address']; ?>",
        phone : "<?php echo $user_data['phone']; ?>",
        username : "<?php echo $user_data['username']; ?>",
        id_rol : <?php echo $user_data['id_rol']; ?>,
        rol : "<?php echo $user_data['rol']; ?>",
      }

      let menus = [
        {title:"Inicio", url:"home"},
        {title:"Artículos Medicamentos", url:"exportar_csv"},
        {title:"Administración de Clientes", url:"clientes"},
      ]

      if (window.info.id_rol == 1) {
        menus.push({title:"Administración de Usuarios", url:"usuarios"})
      }

      $("#auth-right").html("")
      for (var i = 0; i < menus.length; i++) {
        var menu = menus[i]
        var tmp = $("#tmp_menu").html()
        $tmp = $(tmp)
        $tmp.find("[data-menu]").attr("href",menu.url).html(menu.title)
        $tmp.appenTo($("#auth-right"))
      }


    });

    </script>
</head>

<body>
  <div id="auth">

    <div class="row h-100">
      <div class="col-lg-2 d-none d-lg-block">
        <div id="auth-right"> </div>
      </div>
      <div class="col-lg-8 col-12">
        <div class="row">
          <div class="col-md-9 d-flex">
            <span>
              <h5> Bienvenido: <?php echo $user_data['name']; ?> </h5>
            </span>
            <span style="margin-left:10px;">
              <h5> Rol: <?php echo $user_data['rol']; ?> </h5>
            </span>
          </div>
          <div class="col-md-3 text-right">
            <a href="<?php echo base_url(); ?>login/logout" class="text-danger">
              Cerrar Sesión
            </a>
          </div>
        </div>

<!-- tempate listado menus -->
<!-- <section id="tmp_menu" style="display:none;">
  <div class="row">
    <div class="col-md-12">
      <a href="#" data-menu></a>
    </div>
  </div>
</section> -->
<!-- <template id="tmp_menu">
  <div class="row">
    <div class="col-md-12">
      <a href="#" data-menu></a>
    </div>
  </div>
</template> -->
